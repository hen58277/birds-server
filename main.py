# Follow Comments Here: https://blog.heron.me/damn-simple-python-web-server-on-heroku-ba67cad2fb18
import cv2
from flask import Flask
from flask_cors import CORS, cross_origin
from flask import request
from PIL import Image
import io
from keras.models import model_from_json

import numpy as np
from keras.utils.data_utils import get_file
import json
import species
import h5py
from keras.models import load_model
print(cv2.__version__)

app = Flask(__name__)
cors = CORS(app)


loaded_model = load_model('compressed/mobilenetv2_model_40.h5')
print("Weights loaded!")

app.config['CORS_HEADERS'] = 'Content-Type'

# https://stackoverflow.com/questions/22947905/flask-example-with-post
@app.route('/', methods=['GET'])
@cross_origin()
def get():
    return "Hello World, again!"

# birds-classifier-server.herokuapp.com/classify
@app.route('/classify', methods=['POST', 'GET'])
@cross_origin()
def post():
    # damit ein Fehler nicht zum Absturz führt
    try:
        data = request.data
        print("datatype:",type(data))
        img = Image.open(io.BytesIO(data)).convert('RGB')
        img = np.array(img)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB) 
        
        img = cv2.resize(img,(224,224))
        # cv2.imwrite('color_img.jpg', img)
        img = np.reshape(img,[1,224,224,3])
        classes = loaded_model.predict(img)
        id = np.argmax(classes, axis = 1) # Number is the label
        print("species: ",species.SPECIES[int(id)])
        highest_classes = (-classes).argsort()[:5][0]
        print("Top 1:",str(species.SPECIES[highest_classes[0]]))
        print("Top 2:",str(species.SPECIES[highest_classes[1]]))
        print("Top 3:",str(species.SPECIES[highest_classes[2]]))
        print("Top 4:",str(species.SPECIES[highest_classes[3]]))
        print("Top 5:",str(species.SPECIES[highest_classes[4]]))
        data = {"winner":str(species.SPECIES[int(id)]), 
        "top2": str(species.SPECIES[highest_classes[1]]),
        "top3":str(species.SPECIES[highest_classes[2]]),
        "top4":str(species.SPECIES[highest_classes[3]]),
        "top5":str(species.SPECIES[highest_classes[4]])}
        print(data)
        response = app.response_class(response=json.dumps(data), status=200, mimetype='application/json')
        return response
    except:
        print("error!")
        return "error"
    

if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=False)

# Neben dem Tutorial beachten: 

# changes:
# git add . && git commit -m "changes" && git push heroku master
# git push heroku master
# https://stackoverflow.com/questions/6842687/the-remote-end-hung-up-unexpectedly-while-git-cloning

# python -m venv venv
# pip install -r requirements.txt