# Follow Comments Here: https://blog.heron.me/damn-simple-python-web-server-on-heroku-ba67cad2fb18
from flask import Flask
from flask_cors import CORS, cross_origin
from flask import request

app = Flask(__name__)
cors = CORS(app)

app.config['CORS_HEADERS'] = 'Content-Type'

# https://stackoverflow.com/questions/22947905/flask-example-with-post
@app.route('/', methods=['GET'])
@cross_origin()
def get():
    return "Hello World, again!"

# birds-classifier-server.herokuapp.com/classify
@app.route('/classify', methods=['POST', 'GET'])
@cross_origin()
def post():
    data = {"some_key":"Kygo"}
    return data

if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=False)